var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var mysql = require('mysql');
api = require('./routes/api');


var app = express();
app.use(bodyParser.urlencoded({extended: true}));
//app.use(bodyParser.json());

/**
 * Configuration
 */

// all environments
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'
 
//app.set('port', process.env.PORT || 3000);
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.static(path.join(__dirname, 'public')));

pool = mysql.createPool({
    connectionLimit: 100, //important
    host: process.env.OPENSHIFT_MYSQL_DB_HOST,
    user     : process.env.OPENSHIFT_MYSQL_DB_USERNAME,
    password : process.env.OPENSHIFT_MYSQL_DB_PASSWORD,
    port     : process.env.OPENSHIFT_MYSQL_DB_PORT,
    database : process.env.OPENSHIFT_APP_NAME,
    debug: false
});

app.get('/', function (req, res) {
    res.render("index");
})

// JSON API
app.get('/api/about', api.about);
app.get('/api/projects', api.projects);
app.get('/api/blog', api.blog);
app.get('/api/blog-post', api.blogPost);

/*var server = app.listen(app.get('port'), function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
})*/

app.listen(server_port, server_ip_address, function () {
  console.log( "Listening on " + server_ip_address + ", server_port " + server_port )
});