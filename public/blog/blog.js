'use strict';

angular.module('myApp.blog', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/blog', {
                    templateUrl: 'blog/blog.html',
                    controller: 'BlogController'
                });
            }])

        .controller('BlogController', ['$scope', '$http', function ($scope, $http) {
                $http({
                    method: 'GET',
                    url: '/api/blog'
                }).
                    success(function (data, status, headers, config) {
                        $scope.data = data.result;
                    }).
                    error(function (data, status, headers, config) {
                        //$scope.name = 'Error!';
                    });
            }]);