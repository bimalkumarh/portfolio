/*
 * Serve JSON to our AngularJS client
 */
exports.about = function (req, res) {
    pool.getConnection(function (err, connection) {
        if (err) {
            connection.release();
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        }

        console.log('connected as id ' + connection.threadId);

        connection.query("SELECT id,page_heading,page_title,page_description FROM portfolio.about_me LIMIT 1", function (err, rows) {
            connection.release();
            if (!err) {
                res.json({"code": 200, "result": rows[0]});
            }
        });

        connection.on('error', function (err) {
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        });
    });
};

exports.projects = function (req, res) {
    pool.getConnection(function (err, connection) {
        if (err) {
            connection.release();
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        }

        console.log('connected as id ' + connection.threadId);

        connection.query("SELECT id,project_title,sub_title,project_description,image_path,created_at,modified_at FROM projects", function (err, rows) {
            connection.release();
            if (!err) {
                res.json({"code": 200, "result": rows});
            }
        });

        connection.on('error', function (err) {
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        });
    });
};

exports.blog = function (req, res) {
    pool.getConnection(function (err, connection) {
        if (err) {
            connection.release();
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        }

        console.log('connected as id ' + connection.threadId);

        connection.query("SELECT blog.id,blog_category.category_name,blog.post_title, blog.post_description, blog.created_at FROM portfolio.blog INNER JOIN blog_category ON blog_category.id=blog.blog_category_id", function (err, rows) {
            connection.release();
            if (!err) {
                res.json({"code": 200, "result": rows});
            }
        });

        connection.on('error', function (err) {
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        });
    });
};

exports.blogPost = function (req, res) {
    var blogId = req.query.blogid;
    pool.getConnection(function (err, connection) {
        if (err) {
            connection.release();
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        }

        console.log('connected as id ' + connection.threadId);

        connection.query("SELECT blog.id,blog_category.category_name,blog.post_title, blog.post_description, blog.created_at FROM portfolio.blog INNER JOIN blog_category ON blog_category.id=blog.blog_category_id WHERE blog.id=?", [blogId],function (err, rows) {
            connection.release();
            if (!err) {
                res.json({"code": 200, "result": rows[0]});
            }
        });

        connection.on('error', function (err) {
            res.json({"code": 100, "status": "Error in connection database"});
            return;
        });
    });
};