'use strict';

angular.module('myApp.home', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/home', {
                    templateUrl: 'home/home.html',
                    controller: 'HomeController'
                });
            }])

        .controller('HomeController', ['$scope', '$http',function ($scope, $http) {
                $http({
                    method: 'GET',
                    url: '/api/about'
                }).
                success(function (data, status, headers, config) {
                    $scope.data = data.result;
                }).
                error(function (data, status, headers, config) {
                    //$scope.name = 'Error!';
                });
            }]);