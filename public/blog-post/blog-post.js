'use strict';

angular.module('myApp.blog-post', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/blog-post/:blogid', {
                    templateUrl: 'blog-post/blog-post.html',
                    controller: 'BlogPostController'
                });
            }])

        .controller('BlogPostController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
                console.log($routeParams.blogid);
                $http({
                    method: 'GET',
                    url: '/api/blog-post',
                    params: {blogid: $routeParams.blogid}
                }).
                    success(function (data, status, headers, config) {
                        $scope.data = data.result;
                    }).
                    error(function (data, status, headers, config) {
                        //$scope.name = 'Error!';
                    });
            }]);