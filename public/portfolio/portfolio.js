'use strict';

angular.module('myApp.portfolio', ['ngRoute'])

        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/portfolio', {
                    templateUrl: 'portfolio/portfolio.html',
                    controller: 'portfolioController'
                });
            }])

        .controller('portfolioController', ['$scope', '$http', function ($scope, $http) {
                $http({
                    method: 'GET',
                    url: '/api/projects'
                }).
                        success(function (data, status, headers, config) {
                            $scope.data = data.result;
                        }).
                        error(function (data, status, headers, config) {
                            //$scope.name = 'Error!';
                        });
            }]);